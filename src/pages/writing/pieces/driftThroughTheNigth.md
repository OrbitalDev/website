---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Drift through the Night"
breakTitle: "Drift through the Night"
publishDate: April 22 2022
id: driftThroughTheNight
---
Silky sound is no more,  
None of the noise that I abhor.  
This is the moment I waited for,  
But why can't this time I savor?  

<br>

All day long, I've wished for this,  
That which my ears can't possibly miss,  
But, alas, the noise in my head  
Continues its ruckus as I drift off to bed  

<br>

Every mistake it starts to list,  
"Am I good enough? Was I made for this?"  
In dread silence I sit as I miss  
Those people who can comfort me best.  

<br>

Eventually, though, I succumb to twilight,  
No more of those doubts of whether I'm bright  
Or of whether all of my choices were right,  
I just silently drift off through the night.  

 