---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "A Monologue of Falsehoods"
breakTitle: "A Mono\u00ADlogue of False\u00ADhoods"
publishDate: Sat Oct 08 2022
id: aMonologueOfFalsehoods
---
*(08.10.22)*
<br>
The words can't come,  
I can't say.  
No way to express it.  
I mean, I can... I will!  
I declare, "Right at this very moment!"  
And blazingly run to flip the switch on the lamp,  
But my very own consciousness pulls the plug.  
What will they say?  
What will they think?  

<br>

How will I say?  
Say that which I think?  
How can I possibly say what I think  
When what I think may change  
in the batting of an eye?  
Couldn't I just grab the parchment of truth?  
Couldn't I grab it with my own fists?  
And with it, abscond to the safe place.  
There, I'd proclaim the truth.  

<br>

But couldn't I just give an ostensible truth?  
Manufactured words.  
You'd believe them, but would I?  
Should I? Would I be satisfied?  
Or buried deep below,  
lurking in an unburnt book,  
Would I remember the things said, drawn, seen?  
Do I want to? Regardless of my choice,  
This truth remains in the deep dark lands.  

<br>

Either we could make a plan,  
Valiant sword, hand in hand  
And fix this world, its stone, its sand,  
In light and in sorrow,  
Always united, forever,  
Or be a lie lying in the depths. 
At least I'd know you weren't upset.  
So I'd venture away from the light.  
From the light I'd depart,  
Riding into the dark,  
Intents unknown, eerily like home,  
Shadowing that which I know.  
A forigner to me.  