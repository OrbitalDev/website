import rss from "@astrojs/rss";

const postImportResult = import.meta.glob("./writing/pieces/*.md", { eager: true });
const posts = Object.values(postImportResult);

export const get = () =>
  rss({
    title: "Judah Brown's Writing",
    description:
      "Judah Brown's more artistic ramblings about life, the universe, and everything.",
    site: import.meta.env.SITE,
    items: posts
      .filter(
        (post) =>
          post.frontmatter != undefined && post.frontmatter.draft != true
      )
      .map((post) => ({
        link: post.url,
        customData: "<author>hello@judahbrown.dev</author>",
        title: post.frontmatter.title,
        pubDate:
          post.frontmatter.pubDate != undefined
            ? post.frontmatter.pubDate
            : new Date(2023, 0),
        content: post.compiledContent(),
      })),
    stylesheet: "/pretty-feed-v3.xsl",
    drafts: false,
  });
