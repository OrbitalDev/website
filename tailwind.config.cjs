/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    fontFamily: {
      mono: ["JetBrains Mono", "monospace"],
      sans: ["Inter", "Noto Sans", "sans"],
    },
    colors: {
      brand: {
        100: "#A0BEDF",
        150: "#91B4D9",
        200: "#6294CA",
        300: "#2B5076",
        400: "#204265",
        500: "#183A5A",
        600: "#0A2945",
        700: "#011E38",
        800: "#00101E",
      },
    },
    extend: {},
  },
  plugins: [],
};
