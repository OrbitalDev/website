---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "A Heart Full of Stars"
breakTitle: "A Heart Full of Stars"
publishDate: April 20 2022
id: aHeartFullOfStars
---
A heart full of stars,  
Some brightly shining  
And others that are  
Sluggishly dying,  
And scattered they are,  
Both near and far,  
But beating as one  
Do they love.  

<br>

A heart full of stars,  
All seen in the night.  
Pulsing like hearts,  
And hoping they might  
One day become  
As if they were one:  
One with that heart  
Holding them together.  

<br>

These beautiful stars,  
Coming closer to you,  
Leap as they are,  
Whilst dancing too.  
And then there you are,  
With a heart full of stars,  
Connected to you  
Through your love.  