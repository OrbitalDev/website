---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Andromeda's Poems"
breakTitle: "Andromeda's Poems"
publishDate: September 13 2023
id: andromedasPoems
---
*introduction*  
<br>
*these are six poems about you*  
*your beauty, your heart, your eyes*  
<br>
*these are poems about me*  
*my love for you, that must be unseen*  

<br>
 
*i. your hazel eyes*  
<br>
your hazel eyes  
pierce through mine  
often, they lock for a short moment  
and i send a jolt of my feelings to you, i hold your gaze  
<br>
your hazel eyes  
i wish i could swim in them forever  
i get lost in the warmth, the invitation  
the opportunity to build new worlds together  
<br>
your hazel eyes  
make me feel at home  
as i rest in the silent embrace  
of your irises so mysteriously alluring  

<br>
 
*ii. the warmth of a heart*  
<br>
when i find myself near you  
i can't help but feel warm inside  
my heart is beating fast  
from the hope of one last glimpse  
<br>
your heart is a star  
emitting waves of light  
<br>
and my heart takes one last glimpse  
it beats for joy, because you are near  
you make my world better  
i can't help but find myself near you  

<br>

*iii. the emptiness of ends*  
<br>
ends are everything but strangers to you and i  
we've known them eternally  
as a saviour and friend  
as a reaper of death set to destroy  
<br>
destroy everything we have ever loved  
destroy these empty ends  
<br>
if we were together  
our love could see no end  
if i could be the one you wanted  
we could dance under stars eternally  
<br>
but these dreams meet empty ends  
and i'm awakened into cold reality  
<br>
and i keep falling down  
we're broken and bruised again  
and i keep falling for you  
but our hearts are scarred by the past  
<br>
and ends effect emptiness in us  
and we're affected by the change  
<br>
child of andromeda  
you've changed me forever  
i will remember you eternally  
past the end of this emptiness  

<br>
 
*iv. the puzzle of us*  
<br>
we've traversed the solar systems  
we've been created of a different essence  
<br>
by chance, we've collided  
by chance, we've shared our stories  
<br>
love is all i have for you  
love of you is forbidden  
<br>
*but what about the astronauts around*  
*but what about what they will say*  
<br>
nothing they say matters  
nothing they even think is right  
<br>
but what about the painter of the cosmos  
but what of his words  
<br>
why was i handed this nebula of energy  
why does it want to reach for you  
<br>
cursed am i, with this choice so great  
cursed am i for wanting you  
<br>
i can't help but wonder why fate put us here  
i can't help but wonder what you think of me  
<br>
perhaps you gaze up at thoughts of me at night  
perhaps you wish i would burn up in an atmosphere already   
<br>
*i'm puzzled at why i feel this way*  
*i'm puzzled about how you feel*  
<br>
please never speak to me again  
please hold me tight and never let go  
<br>
love me until the day i fall  
loving me is a death wish  
<br>
travel the universe with me  
travel to a point of no return  

<br>
 
*v. why can't i help*  
<br>
*i have no friends*  
but i'm sitting right here  
*but no one cares*  
are we meant to be?  
<br>
no one sees my tears  
except for the sun shining through the moon  
as the drops of moonlight  
meet my beads of salt water  
falling on the ground as i say  
*no one wants to be my friend*  
even though i know i have friends  
could it be that i'm just an asteroid  
visiting this forsaken galaxy  
<br>
some planets see me  
but only the threat within  
they stay close  
but take no chance to join the orbit  
<br>
why can't i help you  
with this feeling i know all too well  
oh, dear child of andromeda  
i love you more than you'll know  
oh, dear song of distance  
we could be together forever  
<br>
but it will never be enough  
for you and me  
we are scarred by the past,  
plain to see  
<br>
our roots are common  
our goals are the same  
and no one is around  
to help rediscover the cosmos  

<br>

*vi. charred pages*
<br>
from now on  
i will speak of us  
as something dead  
<br>
as of now  
i cannot trust  
thoughts from my own head  
<br>
all i hear  
are thoughts of us  
and bits of trinkets i have read  
<br>
burning our book  
is the only way to get over this  
these unmet feelings  
that fall on deaf ears like a love song  
<br>
because that's what this is  
i'm in love but i don't want to be  
because i'm stuck with this curse  
to love the child of andromeda  