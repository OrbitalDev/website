export let links: Array<{ name: string, url: string, icon: string, username?: string, comment?: string }> = [
   {
    name: "Github",
    icon: "GithubIcon",
    url: "https://github.com/velocitydesign/",
    username: "@VelocityDesign"
   },
   {
    name: "Mastodon",
    icon: "MastodonIcon",
    url: "https://fosstodon.org/@OrbitalDev",
    username: "@OrbitalDev@fosstodon.org"
   },
   {
    name: "Matrix",
    icon: "MatrixIcon",
    url: "https://matrix.to/#/@OrbitalDev:matrix.org",
    username: "@OrbitalDev:matrix.org"
   },
   {
    name: "Dribbble",
    icon: "DribbbleIcon",
    url: "https://dribbble.com/orbitaldev/",
    username: "@OrbitalDev"
   },
   {
    name: "PGP Key",
    icon: "KeyIcon",
    url: "/keys/JudahBrown.pgp",
    username: "C07DE063E9C0D1F5"
   },
   {
    name: "Codeberg",
    icon: "GitIcon",
    url: "https://codeberg.org/@OrbitalDev",
    username: "@OrbitalDev"
   },
   {
    name: "Soundcloud",
    icon: "SoundcloudIcon",
    url: "https://soundcloud.com/velocitydesign",
    username: "@VelocityDesign"
   },
   {
    name: "Spotify",
    icon: "SpotifyIcon",
    url: "https://open.spotify.com/user/jn8l1quldtwcmhmjpv0exccwc",
   },
   {
    name: "Youtube",
    icon: "YoutubeIcon",
    url: "https://www.youtube.com/channel/UCfZSeycLtZ0BmD1TqK183lw",
    username: "@OrbitalDev"
   },
]

export default links