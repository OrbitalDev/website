---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "To Love Myself"
breakTitle: "To Love \u00AD Myself"
publishDate: Mon Jan 16 2023
id: toLoveMyself
---
I  
Will  
Never  
Be enough  
To hold all the stars  
To master the breeze in the sky  
To tame the waves from the watery depths below me  
And I worry no one will love me when I can no longer love my own self  

### Footnotes
This is a fibonacci poem.