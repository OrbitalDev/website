---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "It Will Get Better"
breakTitle: "It Will Get \u00AD Better"
publishDate: Sat Jan 07 2023
id: toLoveMyself
---
I tell everyone else it will get better  
But who will tell me when I don't believe it?  
I used to see us as something much brighter,  
But the shades hid lies, seemingly for better,  
Now, I sit alone through the dark, shadowed night  
Wishing on stars for it all to get better.  
I lie down, succumbing to the dreams of light,  
Maybe I can figure myself out better?  
I see you and hear your voice inside my mind,  
Comforting me, saying, "It will get better."  

### Footnotes
No matter what you're going through, it will get better.