---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Moonward Musings"
breakTitle: "Moonward Musings"
publishDate: September 10 2023
id: moonwardMusings
---
**i. a luminous letter**  
<br>
Dear little Leo, I write this to you:  
Always remember to look at the moon.  
For when the sun dips below the horizon,  
There remain countless stars to fix your eyes on.  

<br>

**ii. follow the master**  
<br>
Create, oh child, create a new art.  
Erect the impossible,  
you're set apart.  
<br>
Compose, young one, compose all your thoughts.  
Sing a new symphony,  
shatter their hearts.  
<br>
Design, dearest, design something new.  
Build from the stardust,  
Paint a new view.  
<br>
Remember, child, remember your name,  
Recall where you started,  
And see where you came  
<br>
**iii. foreknown, fateful, just remain faithful**  
<br>
Why do you cry?  
Why do you lie  
Awake at night,  
Observing the sky?  
<br>
Why do you worry?  
Why do you wait?  
Why do you think  
That you control fate?  
<br>
The stars shine on,  
Giving no thought  
To where their light goes:  
They consider it not.  
<br>
The world still spins  
As if it's endued,  
And it will spin on  
Until time is through.  
<br>
Child, remember,  
You can't control fate.  
That job belongs  
To the one who creates  
<br>
He's seen it all  
Since the beginning  
And because of him  
The world keeps spinning.  
<br>
Lo, do remember,  
He sees even you,  
And that you're designed  
And purposeful too.  