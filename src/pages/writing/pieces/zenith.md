---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Zenith"
breakTitle: "Zenith"
publishDate: Wed Jan 25 2023
id: zenith
---
zenith (/ˈziːnɪθ/) - noun. 1) the point of the celestial sphere that is directly above the observer. 2) the peak of something.

<br>

Are we already falling apart  
Are we about to meet our zenith  
We're a lovely work of art,  
Just think of it for a minute

<br>  

You're the star that blinds my eyes,  
You're the star that crowns the skies  
And I'm at the end of a telescope,  
And I'm light years away.

<br>

I look to you and feel dismay  
I wish we could meet before the day  
The day when it'll be too late  
The day when we meet our zenith