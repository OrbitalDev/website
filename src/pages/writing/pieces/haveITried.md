---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Have I Tried"
breakTitle: "Have I Tried"
publishDate: January 16 2024
id: haveITried
---
Underneath a white, snowy sky, do I  
Take a final breath of pure, rare freedom, do I  
Want to run like I am five too, do I  
Frolic around, just with sweaters down, do I try?  
<br>
Watching all the other children, will I  
Leap with faith into the cold, white blanket, will I  
Take this one day for granted, or will I  
Let this moment fade away to time, will I try?  
<br>
Then, with sun shone bright in the cold, have I  
Done that which I want to do within snow, have I  
Caused avalanches in my own life, have I  
Encapsulated time in memories, have I tried?  