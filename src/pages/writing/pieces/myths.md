---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Myths"
breakTitle: "Myths"
publishDate: February 29 2024
id: myths
---
Am I a ship of Theseus,  
The sum of broken parts?  
I traded who I used to be,  
Destroying my old art.  
<br>
Murdering my very heart,  
For Hyacinth is dead,  
None opposing Fate's own hand,  
To love, to kill instead.  
<br>
Do I mimic Icarus,  
Longing to touch the sun?  
Tragedy came from a wish,  
Destined ere it begun.  
<br>
Horses galloping freely,  
Phaethon falling down,  
Someone so much greater claims  
It's best for me to drown  
<br>
Am I like Odysseus,  
So distant from my heart?  
Sailing off, only to fight,  
And growing far apart  
<br>
Glancing back like Orpheus  
Now Eurydice is doomed,  
Further down, forever gone,  
All hope has been consumed  
<br>
Do I reflect Pandora,  
Have I opened the box?  
Unleashing certain evil,  
That was kept under a lock.  
<br>
Laughs of summer wholly fade,  
Echoing through my thoughts.  
A drink from river Lethe,  
Forget all I was taught.  
<br>
Do I appear as Hades,  
Stealing another's joy?  
Countless souls all hating me,  
For I, death, do employ.  
<br>
Winter's hold ensuing now,  
All mourning this great loss,  
Follow her, Persephone  
And sail the sea across  
<br>
Do I stand among these myths  
And find myself in them?  
Tales of monsters, soldiers past,  
Each one a precious gem.  
<br>
The difference is these anecdotes  
Tell of those who are dead.  
Unlike them, I'm still alive,  
This story's mine to give.  
<br>
Will I use this chance of mine  
To make my story whole?  
The future lies ahead of me,  
New history untold.  
<br>
Days anew now dawning here,
A life before me lies,  
Journeys left for me to write  
My myth before I die.  