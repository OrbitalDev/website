---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Box of Memories"
breakTitle: "Box of Memories"
publishDate: Dec 04 2022
id: boxOfMemories
---
Here is a box  
Of memories for you.  
I've counted them all  
And polished them too.  

<br>

Collected treasures  
From such precious times  
Shan't collect dust,  
For that'd be a crime.  

<br>

Let us share them,  
And unite again as one,  
Enjoying our memories  
Under a harsh winter's sun  