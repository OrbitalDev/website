---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Fast Forward"
breakTitle: "Fast Forward"
publishDate: Dec 24 2022
id: fastForward
---
I showed up to the mysterious place.  
Kids were inside, playing, talking, walking.  
A new person came to me. I shied back.  
She held out her hand. I moved close to her.  

<br>

I put monkey in the box with the rest of my stuff.  
Reaching down, I grabbed a slice of a new food. Cookie cake.  
I sat in my seat as we drove westward, enjoying my new treat.  
It was gone in the blink of an eye.  

<br>

I was at a place.  
It was somewhat familiar, but unlike the vague memories.  
We sat together as friends,  
But as soon as I could think, we were down the road again.  

<br>

Nervousness filled me.  
I knew not what to do, or to say, or to think.  
But this was a temporary place,  
One of memories, good and painful.  

<br>

I grew up.  
It was crazy, in a good way.  
I built, saw, and shed.  
We were.

<br>

It was home now, but in a new sense.  
A new start, a new chance.  
But so much time has passed since the past.  