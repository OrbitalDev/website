import { defineConfig } from "astro/config";
import svelte from "@astrojs/svelte";
/* import webfinger from "astro-webfinger";
import compress from "astro-compress"; */
import tailwind from "@astrojs/tailwind";
import Icons from 'unplugin-icons/vite';

// https://astro.build/config
export default defineConfig({
  site: "https://judahbrown.dev",
  integrations: [
    tailwind({
      config: { path: "./tailwind.config.cjs" },
    }),
    svelte(),
    /* webfinger({
      instance: "fosstodon.org",
      username: "OrbitalDev",
    }),
    compress(), */
  ],
  vite: {
    plugins: [
      Icons({
        compiler: 'astro',
      }),
    ],
  },
});
