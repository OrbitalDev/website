---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "My first post for #100DaysToOffload!"
breakTitle: "My first post for #100\u00ADDays\u00ADTo\u00ADOffload!"
publishDate: Wed Jan  4 08:26:12 PM CST 2023
id: 100DaysToOffload
---

# Hey, I'm Judah.
I'm a random guy, and this is my website, including my blog. For a while, I've had a blog "up," but I haven't been actively posting. My goal with #100DaysToOffload is to build a new habit of posting on a regular basis and to create more content for my blog.