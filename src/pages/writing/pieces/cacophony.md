---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Cacophony"
breakTitle: "Caco\u00ADphony"
publishDate: Nov 11 2022
id: cacophony
---
*Cacophony - n. dissonance.*

<br>

A cacophony of sounds  
Arises from my head  
Every single night  
That I lay limp and dead  
Waiting to be heard  
By someone who will listen  
But more likely than not,  
I'll just keep them hidden.  

<br>

A cacophony of thoughts  
Rise up from the dust  
From many more like me:  
Innately wanderlust.  
Sojourning to thrones  
With many, endless cheers,  
But feeling all alone  
With no one clasping near.  