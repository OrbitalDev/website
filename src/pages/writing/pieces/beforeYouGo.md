---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Before You Go"
breakTitle: "Before You Go"
publishDate: February 19 2022
id: beforeYouGo
---
My wrist buzzed. It was him. It had been forever since we last talked. By forever, I mean only a day-ish, but we normally talked every few hours. Something must have be wrong. That, or he was just busy. I hoped for the latter. I quickly answered my phone, longing for even a miniscule dribble of his wonderful conversation.

"Hey!" I excitedly shouted into the phone. His reply was rather bland and mysterious.

"Laura!" he replied ecstatically. "I mean," he paused. "Laura, I really need to talk to you. Meet me at the library tomorrow."

"Library? Tomorrow? Wha-" I started. I was interrupted by a rather blunt and rude disconnect tone. What was going on? I tried calling him back, but to my despair, he didn't respond. I guess I was waiting until tomorrow.

It was a long, torturous night. I tossed left. I turned right. I squirmed in all directions. There I sat for the rest of the night, in my bed, all alone, with just my thoughts and my barely-awake body, thinking about what he could possibly have to say to me. I also thought of the old memories shared between us. I recounted the day I met him under the owk tree, and the time he engraved our names to signify our love. His loving, silky words to me played on repeat in my head. That, and that alone, gave me comfort for the rest of the night.

The dawn could not come any sooner. Quickly, I packed a bag of my essentials. Into my bag, I tossed some snacks and an extra pair of clothes to wear, just in case. Chris was known for being adventurous. I finished my preparing, and headed out my door. As I walked, I passed the lovely lilacs and benches that were adjacent to the sidewalk. Then, it finally came into my sight. There it was. There was the library.

I rushed inside. It took a minute, but I finally spotted him among the chatting crowds of people. I gracefully yet hurriedly walked towards him.

"Follow me," he whispered into my ears. I knew it. Chris had something planned. He rushed forwards and almost got out of my sight. Luckily, he didn't go too far, and I caught up to him shortly after.
   
"Where are we going," I inquired, "And why did you want to meet me?"

"It's a surprise," he replied, "you wouldn't want to ruin that, would you?" I groaned, annoyed. Chris knew I hated surprises. I never loved them, and I never will. Then, I saw it, in all it's wondrous and beautiful glory. In front of us was the owk tree, the very one which I met him at.

"Chris!" I exclaimed, "wha- why- how?"  I stammered, stunned at the appearance. "I thought it was chopped down!"

"I know, I know, but look at this!" He pointed to a small figure carved into the trunk. It was Berty. Berty was his character that he drew in all his best comic strips. "I saw it when-" he started, but suddenly stopped. "Well, that's what I actually needed to talk to you about."

"What? What does the owk tree have to do with anything?" Turns out, it had barely anything to do with the news.

"Never mind that, we need to talk." he meekly said. Something had to be wrong. Chris was one of the most bold and brave people I knew. "I've been asked to-  Well, I can't exactly tell you, because it's confidential."

"Confidential?" I speculated.

"Yes," he continued, "extremely confidential. Nonetheless, it still means... It means I have to leave you."

"WHAT?" I said, expressing the thousands of emotions I was feeling in my brain. My heart pumped at an exceedingly high rate. My face went numb. My brain could think no more.

"I have to go, Laura," he stated matter-of-fact as he tenderly touched my shoulder. "We'll meet again, no matter what. I must leave, and I have to hurry, but I have one last thing to say." He lied. He had two more things to say. "I'll be back soon-" he started.

"Soon? How soon?" I demandingly asked.

"I can't tell you, because it's confidential as well, and I frankly don't know yet." My heart sank. "But you have to promise me one thing. You'll meet me here at the oak tree whenever I should return.

"I promise, but why do you have to leave?" I sobbed.

"You'll see. Goodbye, Laura." he said. He promptly kissed me on the cheek, and left. There I sat, as alone as the night before, just me, my thoughts, and the loving kiss on my cheek.

### Footnotes
A preface to "We'll Meet Again" by TheFatRat. Written from the perspective of the girl singing in the song. For the storytelling purpose, I've used the names Laura (For the girl) and Chris (For the one she is singing to/about), despite the song's lack of character naming. Also, the use of Owk is intentional.