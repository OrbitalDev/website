---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Ninth from the Sun"
breakTitle: "Ninth from the Sun"
publishDate: Sep 01 2022
id: ninthFromTheSun
---
I am Pluto, the celestial being they call a dwarf planet.  

<br>

I.  
Center of attention, yet disregarded by all.  
Distant from sun, for whom I fall.  
Longing to meet her warm embrace,  
Instead, meeting coolness on my round face.  

<br>

II.  
They said I wasn't good enough,  
And I took it rough,  
But I'm bold and I'm brave,  
I don't have to be buff.  
They treated me then as if I was a foe,  
But to them, my self-worth is not something I owe.  
I don't care if they hate me and push me down low,  
I'm good enough just by saying so.  

<br>

III.  
Can't let it get out,  
"Hold in your self-doubt!"  
But I'm worried, at this rate I'll surely blow out!  
So I look to the sun,  
with her rays brightly shown.  
I've learned that with her, I am never alone.  

<br>

IV.  
I'll dare to fly out of the shadows,  
I'll risk it, fly into the light!  
I know where ever I go,  
I won't be alone in the night.  

### Footnotes
This piece may also be called "In my Heart"