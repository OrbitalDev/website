import rss from "@astrojs/rss";

const postImportResult = import.meta.glob("./{writing,blog}/{posts,pieces}/*.md", { eager: true })
const posts = Object.values(postImportResult);

export const get = () =>
  rss({
    title: "Judah's Everything Feed",
    description:
      "A feed of my pieces and my blog all in one.",
    site: import.meta.env.SITE,
    items: posts
      .filter(
        (post) =>
          post.frontmatter != undefined && post.frontmatter.draft != true
      )
      .map((post) => ({
        link: post.url,
        customData: "<author>hello@judahbrown.dev</author>",
        title: post.frontmatter.title,
        pubDate:
          post.frontmatter.pubDate != undefined
            ? post.frontmatter.pubDate
            : new Date(2023, 0),
        content: post.compiledContent(),
      })),
    stylesheet: "/pretty-feed-v3.xsl",
    drafts: false,
  });
