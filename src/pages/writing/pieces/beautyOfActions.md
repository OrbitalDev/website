---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Beauty of Actions"
breakTitle: "Beauty\u00AD of\u00AD Actions"
publishDate: Sat Jan 14 11:42:50 PM CST 2023
id: beautyOfActions
---
The way you look at the world

The way you look at its beauty

Beauty is real

Beauty is found in you

You write wondrous things

You deny that you write

Write your thoughts

Write your feelings

Feelings unseen

Feelings unheard

Unheard voice

Unheard songs

Songs that make us laugh

Songs that I cry to

To ponder thoughts

To ponder words

Words we said

Words that bring joy

Joy is always common

Joy is rare

Rare moments like these

Rare steak, please

Please let me see you again

Please give those deserving a hard kick

Kick a table leg

Kick myself

Myself, a wandering star

Myself, a potential human

Human, we all are

Human, how peculiar

Peculiar roles

Peculiar lines

Lines: read between them

Lines are made for a reason

Reason to live

Reason to love

Love is patient

Love is kind

Kind of okay

Kind of weird

Weird is okay

Weird is great

Greatly appreciated

Greatly underestimated

Underestimated actions

Underestimated abilities

Actions noble

Actions true

Noble

True

# Footnotes
I wrote this blitz poem.

I guess it's kind of okay.

It was fun to write.

Hopefully you liked my words,

Hopefully you noticed this.


(This being the fact that the footnotes themselves are a tanka :P) 
