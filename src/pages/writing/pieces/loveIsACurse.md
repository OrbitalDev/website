---
layout: ../../../layouts/post.astro
# (Use \u00AD to insert a proper place for a line break.)
title: "Love is a Curse"
breakTitle: "Love is a Curse"
publishDate: April 11 2023
id: loveIsACurse
---
Love's curse has afflicted me  
His hold is growing like a tree  
Roots still spiraling deep within  
Blossoms gone with the fast wind  
The outside's rough from the bark  
Oh, I wish I killed it from the start  
<br>
But now it's home to birds and leaves  
And it's a jail to those like me  
<br>
I'm withering away  
I cannot face the day  
Its sad machine will play  
Forever and ever  
And evermore again  